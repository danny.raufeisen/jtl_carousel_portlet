<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace Plugin\jtl_carousel_portlet\Portlets\Carousel;

use JTL\OPC\InputType;
use JTL\OPC\Portlet;
use JTL\OPC\PortletInstance;

/**
 * Class Carousel
 * @package Plugin\jtl_carousel_portlet\Portlets\Carousel
 */
class Carousel extends Portlet
{
    /**
     * @return array
     */
    public function getPropertyTabs(): array
    {
        return [
            'Flags'      => ['indicators', 'keyboard', 'pause', 'touch'],
            __('Styles') => 'styles',
        ];
    }

    /**
     * @return array
     */
    public function getPropertyDesc(): array
    {
        return [
            'num-slides' => [
                'type'  => InputType::NUMBER,
                'label' => __('numSlides'),
                'default' => 3,
                'width' => 33,
            ],
            'interval' => [
                'type'  => InputType::NUMBER,
                'label' => __('intervalMs'),
                'default' => 5000,
                'width' => 66,
            ],
            'indicators' => [
                'type' => InputType::CHECKBOX,
                'label' => __('indicators'),
                'default' => false,
                'width' => 25,
            ],
            'keyboard' => [
                'type' => InputType::CHECKBOX,
                'label' => __('keyboardControls'),
                'default' => false,
                'width' => 25,
            ],
            'pause' => [
                'type' => InputType::CHECKBOX,
                'label' => __('pauseOnHover'),
                'default' => true,
                'width' => 25,
            ],
            'touch' => [
                'type' => InputType::CHECKBOX,
                'label' => __('touchSwipe'),
                'default' => true,
                'width' => 25,
            ],
        ];
    }
}