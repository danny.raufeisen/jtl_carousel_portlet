<h4>Individuelles Config-Panel</h4>

{foreach $portlet->getPropertyDesc() as $key => $desc}
    <div>
        <label>
            {$desc.label}
            <input type="text" id="config-{$key}">
        </label>
    </div>
{/foreach}