{$carouselId = "carousel-{$instance->getUid()}"}

<div id="{$carouselId}" class="OPC-carousel carousel slide" data-ride="carousel"
     data-interval="{if $isPreview}false{else}{$instance->getProperty('interval')}{/if}"
     data-keyboard="{if $instance->getProperty('keyboard')}true{else}false{/if}"
     data-touch="{if $instance->getProperty('touch')}true{else}false{/if}"
     style="{$instance->getStyleString()}">
    {if !$isPreview && $instance->getProperty('indicators')}
        <ol class="carousel-indicators">
            {for $i = 1 to $instance->getProperty('num-slides')}
                <li data-target="#{$carouselId}" data-slide-to="{$i - 1}" {if $i === 1}class="active"{/if}></li>
            {/for}
        </ol>
    {/if}
    <div class="carousel-inner">
        {for $i = 1 to $instance->getProperty('num-slides')}
            {$areaId = 'slide-'|cat:$i}
            <div class="opc-area carousel-item {if $i == 1}active{/if}" data-area-id="{$areaId}">
                {if $isPreview}
                    {$instance->getSubareaPreviewHtml($areaId)}
                {else}
                    {$instance->getSubareaFinalHtml($areaId)}
                {/if}
            </div>
        {/for}
    </div>
    <a class="carousel-control-prev" href="#{$carouselId}" role="button" data-slide="prev">
        <i class="fas fa-chevron-left fa-2x"></i>
    </a>
    <a class="carousel-control-next" href="#{$carouselId}" role="button" data-slide="next">
        <i class="fas fa-chevron-right fa-2x"></i>
    </a>
</div>